﻿[Cmdletbinding()]
param (
    [Parameter(Position=0)]
    [bool]$install = $true,
    [Parameter(Position=1)]
    [switch]$Force
)


if ($install){
    [string]$setting1 = 'DISABLE'
    [string]$setting2 =  'ON'
} else {
    [string]$setting1 = 'ENABLE'
    [string]$setting2 =  'OFF'
}
    
    bcdedit /set LOADOPTIONS "$setting1"_INTEGRITY_CHECKS
    bcdedit /set TESTSIGNING $setting2
    bcdedit /set NOINTEGRITYCHECKS $setting2

if ($Force){
    Restart-Computer -ComputerName $env:COMPUTERNAME -Force
}