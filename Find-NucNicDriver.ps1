﻿[Cmdletbinding()]
param (
    [Parameter(Position=0)]
    [ValidateScript({Test-Path -Path $_})]
    $DriverPath = 'C:\Drivers\NIC'  
)

Write-Verbose "Your drivers are located under '$DriverPath'"
#Find the NICs on your system that have not yet been installed through Device Manager
$Nics = Get-CimInstance -ClassName Win32_PnpEntity | Where-Object {$_.ConfigManagerErrorCode -ne 0 -AND $_.Name -like 'Ethernet*' -OR $_.Name -like 'Network*'} | Select-Object Name,DeviceID


foreach ($Nic in $Nics) {
    Write-Verbose "Processing NIC '$($Nic.Name)'"
    #Get only the Vendor ID + Device ID
    $DeviceID = (($Nic.DeviceID.Split('&'))[0].Split('\')[1])+'&'+($Nic.DeviceID.Split('&'))[1]
    Write-Verbose "NIC '$($Nic.Name)' device ID is '$DeviceID'"
    #Find the Device ID in driver files, filtering on NDIS65 (Win10/2016) and gigabit NICs
    $Files = Get-ChildItem -Path $DriverPath -Recurse | Select-String -Pattern $DeviceID | Group-Object Path | Select-Object Name | Where-Object {$_.Name -like '*NDIS65*' -AND $_.Name -like '*PRO1000*'}

    foreach ($File in $Files){
        $properties = @{
            Nic = $($Nic.Name);
            DeviceID = $DeviceID;
            DriverFile = $File.Name
        }
        $obj = New-Object -TypeName PSCustomObject -Property $properties
        $obj
    }
}